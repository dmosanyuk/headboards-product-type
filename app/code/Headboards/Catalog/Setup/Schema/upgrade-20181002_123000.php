<?php
/**
 * Schema setup.
 *
 * @category  Headboards
 * @package   Headboards\Catalog
 * @author    Dmitry Mosaniuk
 * @copyright 2018 Dmitry_Mosaniuk
 */

/** @var Headboards\Catalog\Setup\InstallSetup $this */
/** @var Magento\Framework\Setup\ModuleDataSetupInterface $setup */
$setup->startSetup();
$this->addExecutionMessage('Headboards_Catalog - Headboard type link setup START');

    //Add store_id to catalog_product_link
    $productLinkTable = 'catalog_product_link';
    $setup->getConnection()
        ->addColumn(
            $setup->getTable($productLinkTable),
            'store_id',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                'nullable' => false,
                'default' => 0,
                'comment' => 'Store Id'
            ]
        );

    /**
     * Install product link types
     */
    $data = [
        ['link_type_id' => \Headboards\Catalog\Model\Catalog\Product\Link::LINK_TYPE_HEADBOARD, 'code' => 'headboardtype']
    ];

    foreach ($data as $bind) {
        $setup->getConnection()
            ->insertForce($setup->getTable('catalog_product_link_type'), $bind);
    }

    /**
     * install product link attributes
     */
    $data = [
        [
            'link_type_id' => \Headboards\Catalog\Model\Catalog\Product\Link::LINK_TYPE_HEADBOARD,
            'product_link_attribute_code' => 'position',
            'data_type' => 'int',
        ]
    ];

    $setup->getConnection()
        ->insertMultiple($setup->getTable('catalog_product_link_attribute'), $data);

$this->addExecutionMessage('Headboards_Catalog - Headboard type link setup END');
$setup->endSetup();
