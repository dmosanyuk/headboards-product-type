<?php
/**
 * Headboards Catalog setup
 *
 * @category  Headboards
 * @package   Headboards\Catalog
 * @author    Dmitry Mosaniuk
 * @copyright 2018 Dmitry_Mosaniuk
 */

/** @var Headboards\Catalog\Setup\InstallSetup $this */
/** @var Magento\Framework\Setup\ModuleDataSetupInterface $setup */

$setup->startSetup();
$this->addExecutionMessage('Headboards_Catalog - Create headboards size attributes START');

    $attributes = [
        'size_name' => [
            'attribute_set'           => 'Headboards',
            'type'                    => 'text',
            'backend'                 => '',
            'frontend'                => '',
            'label'                   => 'Headboard Size Name',
            'input'                   => 'text',
            'class'                   => '',
            'source'                  => '\Magento\Eav\Model\Entity\Attribute\Source\Table',
            'global'                  => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
            'group'                   => 'General',
            'visible'                 => true,
            'required'                => false,
            'user_defined'            => false,
            'default'                 => '',
            'searchable'              => true,
            'filterable'              => true,
            'comparable'              => true,
            'visible_on_front'        => true,
            'used_in_product_listing' => true
        ],
        'a_width' => [
            'attribute_set'           => 'Headboards',
            'type'                    => 'text',
            'backend'                 => '',
            'frontend'                => '',
            'label'                   => 'A-Width',
            'input'                   => 'text',
            'class'                   => '',
            'source'                  => '\Magento\Eav\Model\Entity\Attribute\Source\Table',
            'global'                  => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
            'group'                   => 'General',
            'visible'                 => true,
            'required'                => false,
            'user_defined'            => false,
            'default'                 => '',
            'searchable'              => true,
            'filterable'              => true,
            'comparable'              => true,
            'visible_on_front'        => true,
            'used_in_product_listing' => true
        ],
        'b_length' => [
            'attribute_set'           => 'Headboards',
            'type'                    => 'text',
            'backend'                 => '',
            'frontend'                => '',
            'label'                   => 'B-Length',
            'input'                   => 'text',
            'class'                   => '',
            'source'                  => '\Magento\Eav\Model\Entity\Attribute\Source\Table',
            'global'                  => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
            'group'                   => 'General',
            'visible'                 => true,
            'required'                => false,
            'user_defined'            => false,
            'default'                 => '',
            'searchable'              => true,
            'filterable'              => true,
            'comparable'              => true,
            'visible_on_front'        => true,
            'used_in_product_listing' => true
        ],
        'c_height' => [
            'attribute_set'           => 'Headboards',
            'type'                    => 'text',
            'backend'                 => '',
            'frontend'                => '',
            'label'                   => 'C-Height',
            'input'                   => 'text',
            'class'                   => '',
            'source'                  => '\Magento\Eav\Model\Entity\Attribute\Source\Table',
            'global'                  => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
            'group'                   => 'General',
            'visible'                 => true,
            'required'                => false,
            'user_defined'            => false,
            'default'                 => '',
            'searchable'              => true,
            'filterable'              => true,
            'comparable'              => true,
            'visible_on_front'        => true,
            'used_in_product_listing' => true
        ],
        'd_headboard' => [
            'attribute_set'           => 'Headboards',
            'type'                    => 'text',
            'backend'                 => '',
            'frontend'                => '',
            'label'                   => 'D-Headboard',
            'input'                   => 'text',
            'class'                   => '',
            'source'                  => '\Magento\Eav\Model\Entity\Attribute\Source\Table',
            'global'                  => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
            'group'                   => 'General',
            'visible'                 => true,
            'required'                => false,
            'user_defined'            => false,
            'default'                 => '',
            'searchable'              => true,
            'filterable'              => true,
            'comparable'              => true,
            'visible_on_front'        => true,
            'used_in_product_listing' => true
        ],
        'e_base' => [
            'attribute_set'           => 'Headboards',
            'type'                    => 'text',
            'backend'                 => '',
            'frontend'                => '',
            'label'                   => 'E-Base',
            'input'                   => 'text',
            'class'                   => '',
            'source'                  => '\Magento\Eav\Model\Entity\Attribute\Source\Table',
            'global'                  => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
            'group'                   => 'General',
            'visible'                 => true,
            'required'                => false,
            'user_defined'            => false,
            'default'                 => '',
            'searchable'              => true,
            'filterable'              => true,
            'comparable'              => true,
            'visible_on_front'        => true,
            'used_in_product_listing' => true
        ],
    ];

$this->installProductAttributes($attributes);

$this->addExecutionMessage('Headboards_Catalog - Create headboards size attributes END');
$setup->endSetup();
