<?php
/**
 * Headboards Catalog setup
 *
 * @category  Headboards
 * @package   Headboards\Catalog
 * @author    Dmitry Mosaniuk
 * @copyright 2018 Dmitry_Mosaniuk
 */

/** @var Headboards\Catalog\Setup\InstallSetup $this */
/** @var Magento\Framework\Setup\ModuleDataSetupInterface $setup */

$setup->startSetup();
$this->addExecutionMessage('Headboards_Catalog - Create price attributes for headboard type START');

    /** @var Magento\Eav\Setup\EavSetupFactory $eavSetup */
    $eavSetup = $this->eavSetup->create(['setup' => $setup]);

    //associate these attributes with headboard product type
    $fieldList = [
        'price',
        'special_price',
        'special_from_date',
        'special_to_date',
        'minimal_price',
        'cost',
        'tier_price',
        'weight',
    ];

    // make these attributes applicable to new product type
    foreach ($fieldList as $field) {
        $applyTo = explode(
            ',',
            $eavSetup->getAttribute(\Magento\Catalog\Model\Product::ENTITY, $field, 'apply_to')
        );
        if (!in_array(\Headboards\Catalog\Model\Product\HeadboardType::TYPE_ID, $applyTo)) {
            $applyTo[] = \Headboards\Catalog\Model\Product\HeadboardType::TYPE_ID;
            $eavSetup->updateAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                $field,
                'apply_to',
                implode(',', $applyTo)
            );
        }
    }

$this->addExecutionMessage('Headboards_Catalog - Create price attributes for headboard type END');
$setup->endSetup();
