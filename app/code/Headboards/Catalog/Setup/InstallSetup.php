<?php
/**
 * Headboards Catalog setup
 *
 * @category  Headboards
 * @package   Headboards\Catalog
 * @author    Dmitry Mosaniuk
 * @copyright 2018 Dmitry_Mosaniuk
 */

namespace Headboards\Catalog\Setup;

use Smile\Setup\Api\Setup\InstallSetupInterface;
use Smile\Setup\Setup\TraitSetup;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Catalog\Model\Product;

/**
 * Class InstallSetup.
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class InstallSetup implements InstallSetupInterface
{
    use TraitSetup;

    /**
     * Eav setup service
     *
     * @var EavSetupFactory
     */
    protected $eavSetup;

    /**
     * Class constructor
     *
     * @param EavSetupFactory    $eavSetupFactory    EavSetupFactory
    */
    public function __construct(
        EavSetupFactory $eavSetupFactory
    ) {
        $this->eavSetup = $eavSetupFactory;
    }

    /**
     * Install product attributes
     *
     * @param array $attributes eav attributes
     */
    public function installProductAttributes($attributes)
    {
        $eavSetup = $this->eavSetup->create();
        foreach ($attributes as $attributeCode => $options) {
            $eavSetup->addAttribute(Product::ENTITY, $attributeCode, $options);

            $attributeSetId = $eavSetup->getAttributeSetId(Product::ENTITY, 'Headboards');

            if ($attributeSetId) {
                continue;
            }

            $eavSetup->addAttributeToSet(
                Product::ENTITY,
                $attributeSetId,
                $options['group'],
                $attributeCode
            );
        }
    }
}
