/**
 * Product add to cart JS
 *
 * @category  Headboards
 * @package   Headboards\Catalog
 * @author    Dmitry Mosaniuk
 * @copyright 2018 Dmitry_Mosaniuk
 */
define([
    "jquery",
    'Magento_Customer/js/customer-data',
    "headboardAddToCart",
    "jquery/ui",
    "mage/translate"
],function($, customerData) {

    $('.headboard-add-to-cart').click(function () {
        var ajaxUrl = window.location.origin + "/hearboard_catalog/steps/headboardAddToCart";
        var headboardParentProductId = $("#headboard-parent-id").val();
        var productId = $("input[name='child-product']:checked").val(); // checked child product id on first step as size

        var options = {};
        $.each($('.headboard-steps-container').find('input.validate:checked'), function(){
            options[$(this).attr('data-option-id')] = $(this).attr('data-option-value-id');
        });

        var qty = $("#headboards-qty").val();

        var jsonParentProductId = JSON.stringify(headboardParentProductId);
        var jsonProductId = JSON.stringify(productId);
        var jsonOptions = JSON.stringify(options);
        var jsonQty = JSON.stringify(qty);

        $.ajax({
            showLoader: true,
            url: ajaxUrl,
            cache: false,
            type: 'POST',
            data: {options: jsonOptions, productId: jsonProductId, qty: jsonQty, headboardParentProductId: jsonParentProductId},
            dataType: "json"
        }).done(function (data) {
            var sections = ['cart'];
            customerData.invalidate(sections);
            location.reload();
        });
    });
});
