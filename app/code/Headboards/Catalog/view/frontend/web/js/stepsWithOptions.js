/**
 * Product custom options JS
 *
 * @category  Headboards
 * @package   Headboards\Catalog
 * @author    Dmitry Mosaniuk
 * @copyright 2018 Dmitry_Mosaniuk
 */
define([
    "jquery",
    "stepsWithOptions",
    "jquery/ui",
    "mage/translate"
], function($) {
    "use strict";

    $.widget('headboards.customOptions', {
        options: {
            step: '#step',
            dataTitle: 'data-title',
            validateInput: '.validate',
            stepsTitleItem: '.steps-title-item',
            stepsTitleItemID: '#steps-title-item',
            headboardNextStep: '.headboard-next-step',
            headboardAddToCart: '.headboard-add-to-cart',
            stepsSummaryActions: '.steps-summary-actions .button',
            dataCurrentStep: 'data-current-step',
            dataStep: 'data-step',
            headboardHeightTable: '.headboard-height-table',
            dataOptionPrice: 'data-option-price',
            headboardStepsPrice: '.headboard-steps-price .price',
            tableRadioOption: '.table-radio-option',
            stepTwoDataOptionsID: 'data-options-id',
            stepTwoOptionsBox: '.step2-options-box',
            summaryStep: '#summary-step',
            stepSummaryValue: '.step-summary-value',
            radioOptionSwitcher: '.radio-option-switcher',
            headboardsQty: '#headboards-qty',
            headboardsQtyButton: '.headboards-qty-btn',
            headboardsMinQty: 1,
            headboardsMaxQty: 9999,
            dataButton: 'data-button',
            dataButtonPlus: '+',
            dataButtonMinus: '-',
            errMsg: '.err-msg',
            firstCheckDisclaimerSelector: '#step3 .headboard-fieldset:nth-child(1) .field:nth-child(2) input.validate:checked',
            secondCheckDisclaimerSelector: '#step3 .headboard-fieldset:nth-child(3) .field:nth-child(2) input.validate:checked',
            stepThreeHint: '.step3-hint'
        },

        _create: function () {
            this._changeTableOptions();
            this._nextStep();
            this._changeOptions();
            this._changeStep();
            this._switchStepTwoOptions();
            this._changeQty();
        },

        _nextStep: function () {
            var summaryStep = this.options.summaryStep,
                tableRadioOption = this.options.tableRadioOption,
                validateInput = this.options.validateInput,
                step = this.options.step,
                stepSummaryValue = this.options.stepSummaryValue,
                headboardsMinQty = this.options.headboardsMinQty,
                headboardNextStep = this.options.headboardNextStep,
                headboardAddToCart = this.options.headboardAddToCart,
                stepsSummaryActions = this.options.stepsSummaryActions,
                dataCurrentStep = this.options.dataCurrentStep,
                stepsTitleItemID = this.options.stepsTitleItemID,
                errMsg = this.options.errMsg,
                showErrMsg = true;

            $(headboardNextStep).click(function () {
                var ajaxUrl = window.location.origin + "/hearboard_catalog/steps/next";
                var productId = $(tableRadioOption + ':checked').val();
                var currentStep = Number($(headboardNextStep).attr(dataCurrentStep));

                if (currentStep === 1) {
                    if (!productId) {
                        if (showErrMsg) {
                            showErrMsg = false;
                            $(errMsg).html($.mage.__('Please select size!')).show(0);
                            setTimeout(function () {showErrMsg = true; $(errMsg).hide();}, 5000);
                        }
                        return;
                    } else {
                        var checkedProductId = 'productId=' + productId;
                        $.ajax({
                            showLoader: true,
                            url: ajaxUrl,
                            cache: false,
                            type: 'POST',
                            data: checkedProductId
                        }).done(function (data) {
                            if (data) {
                                $(step + '1').attr('hidden', true).removeClass("active");
                                $(data.blockTwo).insertAfter(step + '1');
                                $(step + '2').addClass('active');
                                $(data.blockThree).insertAfter(step + '2').attr('hidden', true);
                                $(data.blockFour).insertAfter(step + '3').attr('hidden', true);
                                $(data.blockFive).insertAfter(step + '4').attr('hidden', true);
                                $(data.blockSix).insertAfter(step + '5').attr('hidden', true);

                                $(headboardNextStep).attr(dataCurrentStep, 2);
                                $(stepsTitleItemID + '2').addClass('active');
                                $(summaryStep + '2').removeClass('hide').addClass('show');
                            }
                        });
                    }
                } else if (currentStep > 1 && currentStep < 6) {
                    var uniqueRadioName = [],
                        stepValidationPassed = false;
                    $.each($(step + currentStep + ' input' + validateInput), function(){
                        if($.inArray( this.name, uniqueRadioName) < 0 ){
                            uniqueRadioName.push(this.name);
                        }
                    });

                    for ( var i = 0, l = uniqueRadioName.length; i < l; i++ ) {
                        if ($(step + currentStep + ' input' + validateInput + '[name="' + uniqueRadioName[i] + '"]:checked').val()) {
                            stepValidationPassed = true;
                        }
                        else {
                            stepValidationPassed = false;
                            break;
                        }
                    }

                    if (!stepValidationPassed) {
                        if (showErrMsg) {
                            showErrMsg = false;
                            $(errMsg).html($.mage.__('Please select options!')).show(0);
                            setTimeout(function () {showErrMsg = true; $(errMsg).hide();}, 5000);
                        }
                        return;
                    } else {
                        showErrMsg = true;
                        $(errMsg).hide();
                        var nextStep = currentStep + 1;
                        $(step + currentStep).attr('hidden', true).removeClass("active");
                        $(step + nextStep).removeAttr('hidden').addClass('active');
                        $(headboardNextStep).attr(dataCurrentStep, nextStep);
                        $(stepsTitleItemID + nextStep).addClass('active');
                        $(summaryStep + nextStep).removeClass('hide').addClass('show');
                    }

                    if (currentStep === 5){
                        $(summaryStep + '6 ' + stepSummaryValue).html(headboardsMinQty);
                        $(headboardAddToCart).prop('disabled', false);
                        $(stepsSummaryActions).addClass('hide');
                    }
                }
            });
        },

        _changeOptions: function () {
            var container = this.element,
                self = this,
                step = this.options.step,
                summaryStep = this.options.summaryStep,
                dataTitle = this.options.dataTitle,
                validateInput = this.options.validateInput,
                headboardNextStep = this.options.headboardNextStep,
                dataCurrentStep = this.options.dataCurrentStep,
                stepSummaryValue = this.options.stepSummaryValue,
                firstCheckDisclaimerSelector = this.options.firstCheckDisclaimerSelector,
                secondCheckDisclaimerSelector = this.options.secondCheckDisclaimerSelector,
                stepThreeHint = this.options.stepThreeHint;

            container.on('click','input' + validateInput, function(){
                var currentStep = Number($(headboardNextStep).attr(dataCurrentStep)),
                    uniqueRadioName = [],
                    currentStepOptions = '';

                $.each($(step + currentStep + ' input' + validateInput), function(){
                    if($.inArray( this.name, uniqueRadioName) < 0 ){
                        uniqueRadioName.push(this.name);
                    }
                });

                for ( var i = 0, l = uniqueRadioName.length; i < l; i++ ) {
                    var currentNameRadio = $(step + currentStep + ' input' + validateInput + '[name="' + uniqueRadioName[i] + '"]:checked');
                    if (currentNameRadio.val()) {
                        if (currentStepOptions === '') {
                            currentStepOptions = currentNameRadio.attr(dataTitle);
                        } else {
                            currentStepOptions = currentStepOptions + '<br/> ' + currentNameRadio.attr(dataTitle);
                        }
                    }
                }

                $(summaryStep + currentStep + ' ' + stepSummaryValue).html(currentStepOptions);
                self._changePrice();

                if (currentStep === 3) {
                    var firstCheckDisclaimer = $(firstCheckDisclaimerSelector).val(),
                        secondCheckDisclaimer = $(secondCheckDisclaimerSelector).val();
                    if (firstCheckDisclaimer && secondCheckDisclaimer) {
                        $(stepThreeHint).removeClass('hide');
                    } else $(stepThreeHint).addClass('hide');
                }
            });
        },

        _changeStep: function () {
            var container = this.element,
                self = this,
                summaryStep = this.options.summaryStep,
                validateInput = this.options.validateInput,
                step = this.options.step,
                headboardsQty = this.options.headboardsQty,
                headboardsMinQty = this.options.headboardsMinQty,
                stepsTitleItem = this.options.stepsTitleItem,
                headboardNextStep = this.options.headboardNextStep,
                headboardAddToCart = this.options.headboardAddToCart,
                stepsSummaryActions = this.options.stepsSummaryActions,
                dataCurrentStep = this.options.dataCurrentStep,
                dataStep = this.options.dataStep,
                stepsTitleItemID = this.options.stepsTitleItemID,
                stepSummaryValue = this.options.stepSummaryValue,
                stepThreeHint = this.options.stepThreeHint;

            container.on('click',stepsTitleItem, function(){
                var currentStep = Number($(headboardNextStep).attr(dataCurrentStep)),
                    selectedStep = Number($(this).attr(dataStep));

                if (selectedStep < currentStep) {
                    $(step + currentStep).attr('hidden', true).removeClass('active');
                    $(step + selectedStep).removeAttr('hidden').addClass('active');
                    $(headboardNextStep).attr(dataCurrentStep, selectedStep);

                    if (selectedStep === 1) {
                        for ( var s = 2, lastStep = 6; s <= lastStep; s++ ) {
                            $(step + s).remove();
                        }
                    }

                    if (selectedStep < 3) {
                        $(stepThreeHint).addClass('hide');
                    }
                }

                for ( var i = selectedStep + 1, l = currentStep + 1; i < l; i++ ) {
                    $(stepsTitleItemID + i).removeClass('active');
                    $(summaryStep + i + ' ' + stepSummaryValue).html('');
                    $(summaryStep + i).removeClass('show').addClass('hide');
                    $(step + i + ' input' + validateInput).prop('checked', false);
                }

                $(headboardsQty).val(headboardsMinQty);
                self._changePrice();
                $(headboardAddToCart).prop('disabled', true);
                $(stepsSummaryActions).removeClass('hide');
            });
        },

        _changeTableOptions: function () {
            var validateInput = this.options.validateInput,
                headboardHeightTable = this.options.headboardHeightTable,
                tableRadioOption = this.options.tableRadioOption,
                stepSummaryValue = this.options.stepSummaryValue,
                summaryStep = this.options.summaryStep,
                dataTitle = this.options.dataTitle,
                step = this.options.step,
                dataOptionPrice = this.options.dataOptionPrice,
                headboardStepsPrice = this.options.headboardStepsPrice,
                selectedFirstStepRadio = $(step + '1 input' + validateInput + ':checked');

            $(summaryStep + '1 ' + stepSummaryValue).html(selectedFirstStepRadio.attr(dataTitle));
            selectedFirstStepRadio.closest('tr').addClass('active');

            var optionPrice = parseFloat($(tableRadioOption + validateInput + ':checked').attr(dataOptionPrice));
            if (optionPrice) {
                $(headboardStepsPrice).html(optionPrice.toFixed(2)
                    .replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
            }

            $(headboardHeightTable + ' tr').click(function() {
                if (!$(this).hasClass('active')) {
                    $(headboardHeightTable + ' tr').removeClass('active');
                    $(this).addClass('active');
                    $(this).find(tableRadioOption).prop('checked', true).trigger( "click" );
                }
            });
        },

        _switchStepTwoOptions: function () {
            var container = this.element,
                radioOptionSwitcher = this.options.radioOptionSwitcher,
                stepTwoDataOptionsID = this.options.stepTwoDataOptionsID,
                stepTwoOptionsBox = this.options.stepTwoOptionsBox;

            container.on('click','input' + radioOptionSwitcher, function(){
                var selectedOptionsID = $(this).attr(stepTwoDataOptionsID);
                $(stepTwoOptionsBox).removeClass('show').addClass('hide');
                $('#' + selectedOptionsID).removeClass('hide').addClass('show');
            });
        },

        _changeQty: function () {
            var container = this.element,
                summaryStep = this.options.summaryStep,
                stepSummaryValue = this.options.stepSummaryValue,
                headboardsQty = this.options.headboardsQty,
                headboardsQtyButton = this.options.headboardsQtyButton,
                headboardsMinQty = this.options.headboardsMinQty,
                headboardsMaxQty = this.options.headboardsMaxQty,
                dataButton = this.options.dataButton,
                dataButtonPlus = this.options.dataButtonPlus,
                dataButtonMinus = this.options.dataButtonMinus;

            container.on('click',headboardsQtyButton, function(){
                var currentQty = parseFloat($(headboardsQty).val());

                if ($(this).attr(dataButton) === dataButtonPlus && currentQty >= headboardsMinQty) {
                    ++currentQty;
                } else if ($(this).attr(dataButton) === dataButtonMinus && currentQty > headboardsMinQty){
                    --currentQty;
                } else currentQty = headboardsMinQty;
                $(headboardsQty).val(currentQty);
                $(summaryStep + '6 ' + stepSummaryValue).html(currentQty);
            });

            container.on('change',headboardsQty, function(){
                if ($(this).val() < headboardsMinQty) {
                    $(this).val(headboardsMinQty);
                    $(summaryStep + '6 ' + stepSummaryValue).html(headboardsMinQty);
                } else if ($(this).val() > headboardsMaxQty) {
                    $(this).val(headboardsMaxQty);
                    $(summaryStep + '6 ' + stepSummaryValue).html(headboardsMaxQty);
                } else $(summaryStep + '6 ' + stepSummaryValue).html($(this).val());
            });
        },

        _changePrice: function () {
            var container = this.element,
                validateInput = this.options.validateInput,
                headboardStepsPrice = this.options.headboardStepsPrice,
                dataOptionPrice = this.options.dataOptionPrice,
                selectedOptionsID = [],
                priceWithOptions = 0;

            $.each(container.find('input' + validateInput + ':checked'), function(){
                selectedOptionsID[selectedOptionsID.length] = $(this).attr('id');
            });

            for ( var i = 0, l = selectedOptionsID.length; i < l; i++ ) {
                var optionPrice = parseFloat($('#' + selectedOptionsID[i]).attr(dataOptionPrice));
                if (optionPrice) {
                    priceWithOptions = priceWithOptions + optionPrice;
                }
            }

            $(headboardStepsPrice).html(priceWithOptions.toFixed(2)
                .replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
        },
    });

    return $.headboards.customOptions;
});
