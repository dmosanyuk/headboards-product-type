<?php
/**
 * Headboards Catalog CollectionProvider HeadboardType.
 *
 * @category  Headboards
 * @package   Headboards\Catalog
 * @author    Dmitry Mosaniuk
 * @copyright 2018 Dmitry_Mosaniuk
 */

namespace Headboards\Catalog\Model\Product\Link\CollectionProvider;

/**
 * Class HeadboardType
 * @package Headboards\Catalog\Model\Product\Link\CollectionProvider
 */
class HeadboardType implements \Magento\Catalog\Model\ProductLink\CollectionProviderInterface
{
    /**
     * {@inheritdoc}
     */
    public function getLinkedProducts(\Magento\Catalog\Model\Product $product)
    {
        $products = $product->getHeadboardTypeProducts();

        if (!isset($products)) {
            return [];
        }

        return $products;
    }
}
