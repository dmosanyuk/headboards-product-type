<?php
/**
 * Headboards Catalog CollectionProvider HeadboardType.
 *
 * @category  Headboards
 * @package   Headboards\Catalog
 * @author    Dmitry Mosaniuk
 * @copyright 2018 Dmitry_Mosaniuk
 */

namespace Headboards\Catalog\Model\Product;

/**
 * Class Price
 * @package Headboards\Catalog\Model\Product
 */
class Price extends \Magento\Catalog\Model\Product\Type\Price
{

}
