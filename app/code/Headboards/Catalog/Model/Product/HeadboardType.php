<?php
/**
 * Headboards Catalog Model HeadboardType.
 *
 * @category  Headboards
 * @package   Headboards\Catalog
 * @author    Dmitry Mosaniuk
 * @copyright 2018 Dmitry_Mosaniuk
 */

namespace Headboards\Catalog\Model\Product;

/**
 * Class HeadboardType
 * @package Headboards\Catalog\Model\Product
 */
class HeadboardType extends \Magento\Catalog\Model\Product\Type\AbstractType
{
    /**
     * Product type ID
     */
    const TYPE_ID = 'headboard';

    /**
     * {@inheritdoc}
     */
    public function deleteTypeSpecificData(\Magento\Catalog\Model\Product $product)
    {
        // method intentionally empty
    }
}
