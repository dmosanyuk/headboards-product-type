<?php
/**
 * Headboards Catalog Model Product.
 *
 * @category  Headboards
 * @package   Headboards\Catalog
 * @author    Dmitry Mosaniuk
 * @copyright 2018 Dmitry_Mosaniuk
 */

namespace Headboards\Catalog\Model;

/**
 * Class Product
 * @package Headboards\Catalog\Model
 */
class Product extends \Magento\Catalog\Model\Product
{
    /**
     * Retrieve array of headboard size products
     *
     * @return array
     */
    public function getHeadboardTypeProducts()
    {
        if (!$this->hasHeadboardTypeProducts()) {
            $products = [];
            foreach ($this->getHeadboardTypeProductCollection() as $product) {
                $products[] = $product;
            }
            $this->setHeadboardTypeProducts($products);
        }

        return $this->getData('headboard_type_products');
    }

    /**
     * Retrieve headboard size products identifiers
     *
     * @return array
     */
    public function getHeadboardTypeIds()
    {
        if (!$this->hasHeadboardTypeProductIds()) {
            $ids = [];
            foreach ($this->getHeadboardTypeProducts() as $product) {
                $ids[] = $product->getId();
            }
            $this->setHeadboardTypeProductIds($ids);
        }

        return $this->getData('headboardtype_product_ids');
    }

    /**
     * Retrieve collection headboard type product
     *
     * @return \Magento\Catalog\Model\ResourceModel\Product\Link\Product\Collection
     */
    public function getHeadboardTypeProductCollection()
    {
        $collection = $this->getLinkInstance()->useHeadboardTypeLinks()->getProductCollection()->setIsStrongMode();
        $collection->setProduct($this);

        return $collection;
    }

    /**
     * Retrieve collection headboard type link
     *
     * @return \Magento\Catalog\Model\ResourceModel\Product\Link\Collection
     */
    public function getHeadboardTypeLinkCollection()
    {
        $collection = $this->getLinkInstance()->useHeadboardTypeLinks()->getLinkCollection();
        $collection->setProduct($this);
        $collection->addLinkTypeIdFilter();
        $collection->addProductIdFilter();
        $collection->joinAttributes();

        return $collection;
    }
}
