<?php
/**
 * Steps Export Products.
 *
 * @category  Headboards
 * @package   Headboards\Catalog
 * @author    Dmitry Mosaniuk
 * @copyright 2018 Dmitry_Mosaniuk
 */

namespace Headboards\Catalog\Model\CatalogImportExport\Export;

/**
 * Class Product
 * @package Headboards\Catalog\Model\CatalogImportExport\Export
 */
class Product extends \Magento\CatalogImportExport\Model\Export\Product
{
    /**
     * Set headers columns
     *
     * @param array $customOptionsData
     * @param array $stockItemRows
     *
     * @return void
     */
    protected function setHeaderColumns($customOptionsData, $stockItemRows)
    {
        if (!$this->_headerColumns) {
            parent::setHeaderColumns($customOptionsData, $stockItemRows);

            $this->_headerColumns = array_merge(
                $this->_headerColumns,
                [
                    'headboardtype_skus',
                    'headboardtype_position'
                ]
            );
        }
    }
}
