<?php
/**
 * Steps Model Proxy.
 *
 * @category  Headboards
 * @package   Headboards\Catalog
 * @author    Dmitry Mosaniuk
 * @copyright 2018 Dmitry_Mosaniuk
 */

namespace Headboards\Catalog\Model\Catalog\Product\Link;

/**
 * Class Proxy
 * @package Headboards\Catalog\Model\Catalog\Product\Link
 */
class Proxy extends \Magento\Catalog\Model\Product\Link\Proxy
{
    /**
     * {@inheritdoc}
     */
    public function useHeadboardTypeLinks()
    {
        return $this->_getSubject()->useHeadboardTypeLinks();
    }
}
