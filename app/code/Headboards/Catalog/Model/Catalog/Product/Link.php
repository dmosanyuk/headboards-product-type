<?php
/**
 * Headboards Catalog Model Product Link.
 *
 * @category  Headboards
 * @package   Headboards\Catalog
 * @author    Dmitry Mosaniuk
 * @copyright 2018 Dmitry_Mosaniuk
 */

namespace Headboards\Catalog\Model\Catalog\Product;

/**
 * Class Link
 * @package Headboards\Catalog\Model\Catalog\Product
 */
class Link extends \Magento\Catalog\Model\Product\Link
{
    /**
     * Headboard product type id
     */
    const LINK_TYPE_HEADBOARD = 6;

    /**
     * Use Headboard Type Links
     *
     * @return \Magento\Catalog\Model\Product\Link $this
     */
    public function useHeadboardTypeLinks()
    {
        $this->setLinkTypeId(self::LINK_TYPE_HEADBOARD);

        return $this;
    }

    /**
     * Save data for product relations
     *
     * @param \Magento\Catalog\Model\Product $product
     *
     * @return \Magento\Catalog\Model\Product\Link
     */
    public function saveProductRelations($product)
    {
        parent::saveProductRelations($product);

        $data = $product->getHeadboardTypeData();
        if (!is_null($data)) {
            $this->_getResource()->saveProductLinks($product->getId(), $data, self::LINK_TYPE_HEADBOARD);
        }
    }
}
