<?php
/**
 * Headboards Catalog block StepsWithOptions.
 *
 * @category  Headboards
 * @package   Headboards\Catalog
 * @author    Dmitry Mosaniuk
 * @copyright 2018 Dmitry_Mosaniuk
 */

namespace Headboards\Catalog\Block;

use Magento\Directory\Model\Currency;
use Magento\Directory\Model\CurrencyFactory;
use Magento\Framework\Pricing\Helper\Data as PricingHelper;
use Magento\Catalog\Helper\Image as ImageHelper;
use Magento\Framework\App\ResourceConnection;
use Magento\Catalog\Model\Product\Option;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Registry;
use \Magento\Framework\View\Element\Template;

/**
 * Class StepsWithOptions
 * @package Headboards\Catalog\Block
 */
class StepsWithOptions extends Template
{
    /**
     * Currency Model.
     *
     * @var $currencyModel
     */
    protected $currencyModel;

    /**
     * Currency Factory.
     *
     * @var $currencyFactory
     */
    protected $currencyFactory;

    /**
     * Pricing Helper.
     *
     * @var $pricingHelper
     */
    protected $pricingHelper;

    /**
     * Image Helper.
     *
     * @var $imageHelper
     */
    protected $imageHelper;

    /**
     * Resource Connection.
     *
     * @var $resourceConnection
     */
    protected $resourceConnection;

    /**
     * Product Option.
     *
     * @var $productOptions
     */
    protected $productOptions;

    /**
     * Product Repository Interface.
     *
     * @var $productRepository
     */
    protected $productRepository;

    /**
     * Registry.
     *
     * @var $registry
     */
    protected $registry;

    /**
     * Data constructor.
     *
     * @param \Magento\Directory\Model\Currency
     * @param \Magento\Directory\Model\CurrencyFactory
     * @param \Magento\Framework\Pricing\Helper\Data
     * @param \Magento\Catalog\Helper\Image
     * @param \Magento\Framework\App\ResourceConnection
     * @param \Magento\Catalog\Model\Product\Option
     * @param \Magento\Framework\Registry
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        Currency $currencyModel,
        CurrencyFactory $currencyFactory,
        PricingHelper $pricingHelper,
        ImageHelper $imageHelper,
        ResourceConnection $resourceConnection,
        Option $productOptions,
        ProductRepositoryInterface $productRepository,
        Registry $registry,
        Template\Context $context,
        array $data = []
    ) {
        $this->currencyModel = $currencyModel;
        $this->currencyFactory = $currencyFactory;
        $this->pricingHelper = $pricingHelper;
        $this->imageHelper = $imageHelper;
        $this->resourceConnection = $resourceConnection;
        $this->productOptions = $productOptions;
        $this->productRepository = $productRepository;
        $this->registry = $registry;
        parent::__construct($context, $data);
    }

    /**
     * Get Headboard Type Linked Products Array According Current Product
     *
     * @param \Magento\Catalog\Model\Product $currentProduct
     *
     * @return array|null
     */
    public function getHeadboardLinkedProducts($currentProduct)
    {
        $linkedProducts = $currentProduct->getHeadboardTypeProducts();
        if ($linkedProducts) {
            $childProducts = [];
            foreach ($linkedProducts as $linkedProduct) {
                $childProducts[] = $this->getProductById($linkedProduct->getId());
            }

            return $childProducts;
        }

        return null;
    }

    /**
     * Get Current Product On Current Product Page
     *
     * @return object
     */
    public function getCurrentProduct()
    {
        return $this->registry->registry('current_product');
    }

    /**
     * Get Headboard Type Child Products Array
     *
     * @param \Magento\Catalog\Model\Product $product
     *
     * @return array|null
     */
    public function getProductOptions($product)
    {
        if ($this->productOptions->getProductOptionCollection($product)) {

            $productOptionsArray = [];

            foreach ($this->productOptions->getProductOptionCollection($product) as $productOption) {
                $productOptionsArray[] = $productOption;
            }

            return $productOptionsArray;
        }

        return null;
    }

    /**
     * Get Array Sorted Option Values By Sku
     *
     * @param \Magento\Catalog\Model\Product\Option $option
     *
     * @return array
     */
    public function getSortedOptionValuesBySku($option)
    {
        $groupedValues = [];
        foreach ($option->getValues() as $optionValue){
            if(isset($groupedValues[$optionValue->getSku()])){
                $groupedValues[$optionValue->getSku()][$optionValue->getOptionTypeId()] = $optionValue->getData();
            }else{
                $groupedValues[$optionValue->getSku()] = [$optionValue->getOptionTypeId() => $optionValue->getData()];
            }
        }

        return $groupedValues;
    }

    /**
     * Get Product By Id
     *
     * @param int $productId
     *
     * @return \Magento\Catalog\Api\Data\ProductInterface|null
     */
    public function getProductById($productId)
    {
        if ($productId) {
            return $this->productRepository->getById($productId);
        }

        return null;
    }

    /**
     * Get Product Image Url
     *
     * @param \Magento\Catalog\Model\Product $product
     * @param int $width
     * @param int $produ$height
     *
     * @return string
     */
    public function getProductImageUrl($product, $width = 150, $height = 150)
    {
        return $this->imageHelper
            ->init($product, 'product_base_image')
            ->constrainOnly(TRUE)
            ->keepAspectRatio(TRUE)
            ->keepTransparency(TRUE)
            ->keepFrame(FALSE)
            ->resize($width, $height)->getUrl();
    }

    /**
     * Get Currency Format
     *
     * @param float $productPrice
     * @param bool  $format
     *
     * @return float
     */
    public function getCurrencyFormat($productPrice, $format = true)
    {
        return $this->pricingHelper->currency($productPrice, $format,false);
    }

    /**
     * Get Headboard Child Products Min Price As Default
     *
     * @param object $currentProduct
     *
     * @return string|null
     */
    public function getHeadboardChildProductsMinPrice($currentProduct)
    {
        $headboardLinkedProducts = $this->getHeadboardLinkedProducts($currentProduct);
        if ($headboardLinkedProducts && ($currentProduct->getTypeId() == \Headboards\Catalog\Model\Product\HeadboardType::TYPE_ID)) {
            $prices = [];
            foreach ($headboardLinkedProducts as $linkedProduct) {
                $prices[] = $linkedProduct->getPrice();
            }
            $minLinkedProductPrice = min($prices);

            return $minLinkedProductPrice;
        }

        return null;
    }

    /**
     * Get Current Currency Symbol
     *
     * @return string
     */
    public function getCurrencySymbol()
    {
        $currency = $this->currencyFactory->create()
            ->load($this->_storeManager->getStore()->getCurrentCurrencyCode());

        return $currency->getCurrencySymbol();
    }

    /**
     * Get Step Titles Array
     *
     * @return array
     */
    public function getStepTitles()
    {
        return [
            'Choose Height',
            'Choose Fabric',
            'Fire retardency',
            'How to attach',
            'Other options',
            'Quantity'
        ];
    }

    /**
     * Get Step Titles Array For Mini Block
     *
     * @return array
     */
    public function getStepTitlesMini()
    {
        return [
            'Height',
            'Fabric',
            'Fire retardency',
            'How to attach',
            'Other options',
            'Quantity'
        ];
    }
}
