<?php
/**
 * Headboards Catalog block Tab.
 *
 * @category  Headboards
 * @package   Headboards\Catalog
 * @author    Dmitry Mosaniuk
 * @copyright 2018 Dmitry_Mosaniuk
 */

namespace Headboards\Catalog\Block\Adminhtml\Catalog\Product\Edit;

/**
 * Class Tab
 * @package Headboards\Catalog\Block\Adminhtml\Catalog\Product\Edit
 */
class Tab extends \Magento\Backend\Block\Widget\Tab
{
    /**
     * Tab Constructor
     *
     * @param \Magento\Backend\Block\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);

        if (!$this->_request->getParam('id') || !$this->_authorization->isAllowed('Magento_Review::reviews_all')) {
            $this->setCanShow(false);
        }
    }
}
