<?php
/**
 * Headboards Catalog DataProvider HeadboardTypeDataProvider.
 *
 * @category  Headboards
 * @package   Headboards\Catalog
 * @author    Dmitry Mosaniuk
 * @copyright 2018 Dmitry_Mosaniuk
 */

namespace Headboards\Catalog\Ui\DataProvider\Product\Related;

use Magento\Catalog\Ui\DataProvider\Product\Related\AbstractDataProvider;

/**
 * Class HeadboardTypeDataProvider
 * @package Headboards\Catalog\Ui\DataProvider\Product\Related
 */
class HeadboardTypeDataProvider extends AbstractDataProvider
{
    /**
     * {@inheritdoc
     */
    protected function getLinkType()
    {
        return 'headboardtype';
    }
}
