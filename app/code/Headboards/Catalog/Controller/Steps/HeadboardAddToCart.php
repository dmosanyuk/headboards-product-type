<?php
/**
 * Steps Controller HeadboardAddToCart.
 *
 * @category  Headboards
 * @package   Headboards\Catalog
 * @author    Dmitry Mosaniuk
 * @copyright 2018 Dmitry_Mosaniuk
 */

namespace Headboards\Catalog\Controller\Steps;

use Magento\Catalog\Model\Product\Exception as ProductException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Controller\ResultFactory;

/**
 * Class HeadboardAddToCart
 * @package Headboards\Catalog\Controller\Steps
 */
class HeadboardAddToCart extends \Magento\Framework\App\Action\Action
{
    /**
     * Cart Repository Interface
     *
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    protected $cartRepository;

    /**
     * Registry.
     *
     * @var $registry
     */
    protected $registry;

    /**
     * Url Interface.
     *
     * @var $urlInterface
     */
    protected $urlInterface;

    /**
     * Checkout Session.
     *
     * @var $checkoutSession
     */
    protected $checkoutSession;

    /**
     * Escaper.
     *
     * @var $escaper
     */
    protected $escaper;

    /**
     * Result Page Factory.
     *
     * @var $resultPageFactory
     */
    protected $resultPageFactory;

    /**
     * Result Json Factory.
     *
     * @var $resultJsonFactory
     */
    protected $resultJsonFactory;

    /**
     * Product Repository Interface.
     *
     * @var $productRepository
     */
    protected $productRepository;

    /**
     * Model Cart.
     *
     * @var $cartModel
     */
    protected $cartModel;

    /**
     * Form Key.
     *
     * @var $formKey
     */
    protected $formKey;

    /**
     * Data constructor.
     *
     * @param \Magento\Quote\Api\CartRepositoryInterface $cartRepository
     * @param \Magento\Framework\UrlInterface $urlInterface
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Framework\Escaper $escaper
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Magento\Checkout\Model\Cart $cartModel
     * @param \Magento\Framework\Data\Form\FormKey $formKey
     * @param \Magento\Framework\App\Action\Context $context
     */
    public function __construct(
        \Magento\Quote\Api\CartRepositoryInterface $cartRepository,
        \Magento\Framework\UrlInterface $urlInterface,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\Escaper $escaper,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Checkout\Model\Cart $cartModel,
        \Magento\Framework\Data\Form\FormKey $formKey,
        \Magento\Framework\App\Action\Context $context
    ) {
        $this->cartRepository = $cartRepository;
        $this->urlInterface = $urlInterface;
        $this->checkoutSession = $checkoutSession;
        $this->escaper = $escaper;
        $this->resultPageFactory = $resultPageFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->productRepository = $productRepository;
        $this->cartModel = $cartModel;
        $this->formKey = $formKey;
        parent::__construct($context);
    }

    /**
     * Ajax request
     */
    public function execute()
    {
        $productId = json_decode(stripslashes($this->getRequest()->getParam('productId')));
        $qty = json_decode(stripslashes($this->getRequest()->getParam('qty')));
        $optionsStdClass = json_decode(stripslashes($this->getRequest()->getParam('options')));
        $options = json_decode(json_encode($optionsStdClass), true);
        $headboardParentProductId = json_decode(stripslashes($this->_request->getParam('headboardParentProductId')));

        $product = $this->productRepository->getById($productId);
        $cart = $this->cartModel;
        $formKey = $this->formKey->getFormKey();

        $params = [
            'form_key' => $formKey,
            'product' => $productId,
            'qty'   => $qty,
            'options' => $options
        ];

        try {
            $cart->addProduct($product, $params);
            $cart->save();

            $quoteItems = $this->cartModel->getQuote()->getAllItems();
            foreach($quoteItems as $quoteItem) {
                if ($quoteItem->getProductId() == $productId) {
                    $quote = $this->cartRepository->get($quoteItem->getQuoteId());
                    $quoteItemId = $quoteItem->getId();
                }
            }
            $item = $quote->getItemById($quoteItemId);
            $item->setData("parent_item_id", $headboardParentProductId);
            $item->save();

            if ($this->checkoutSession->getQuote()->hasProductId($product->getId())) {
                $successMessage = __(
                    'You added %1 to your shopping cart.',
                    $this->escaper->escapeHtml($product->getName())
                );
                $this->messageManager->addSuccessMessage($successMessage);
            }

        } catch (ProductException $e) {
            $this->messageManager->addErrorMessage(__('This product(s) is out of stock.'));
        } catch (LocalizedException $e) {
            $this->messageManager->addNoticeMessage($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addExceptionMessage($e, __('We can\'t add the item to the cart right now.'));
        }
    }
}
