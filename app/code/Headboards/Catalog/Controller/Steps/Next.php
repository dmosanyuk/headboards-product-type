<?php
/**
 * Steps Controller Next.
 *
 * @category  Headboards
 * @package   Headboards\Catalog
 * @author    Dmitry Mosaniuk
 * @copyright 2018 Dmitry_Mosaniuk
 */

namespace Headboards\Catalog\Controller\Steps;

/**
 * Class Next
 * @package Headboards\Catalog\Controller\Steps
 */
class Next extends \Magento\Framework\App\Action\Action
{
    /**
     * Result Page Factory.
     *
     * @var $resultPageFactory
     */
    protected $resultPageFactory;

    /**
     * Result Json Factory.
     *
     * @var $resultJsonFactory
     */
    protected $resultJsonFactory;

    /**
     * Product Repository Interface.
     *
     * @var $productRepository
     */
    protected $productRepository;

    /**
     * Checkout Model Cart.
     *
     * @var $cart
     */
    protected $cart;

    /**
     * Data constructor.
     *
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Magento\Framework\App\Action\Context $context
     */
    public function __construct(
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\App\Action\Context $context
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    /**
     * Ajax request
     *
     * @return \Magento\Framework\Controller\Result\Json|null
     */
    public function execute()
    {
        $headboardChildId = $this->getRequest()->getParam('productId');

        if ($headboardChildId) {
            $result = $this->resultJsonFactory->create();
            $resultPage = $this->resultPageFactory->create();

            $blockTwo = $resultPage->getLayout()
                ->createBlock('Headboards\Catalog\Block\StepsWithOptions')
                ->setTemplate('Headboards_Catalog::product/view/step_two.phtml')
                ->setData('child_id', $headboardChildId)
                ->toHtml();

            $blockThree = $resultPage->getLayout()
                ->createBlock('Headboards\Catalog\Block\StepsWithOptions')
                ->setTemplate('Headboards_Catalog::product/view/step_three.phtml')
                ->setData('child_id', $headboardChildId)
                ->toHtml();

            $blockFour = $resultPage->getLayout()
                ->createBlock('Headboards\Catalog\Block\StepsWithOptions')
                ->setTemplate('Headboards_Catalog::product/view/step_four.phtml')
                ->setData('child_id', $headboardChildId)
                ->toHtml();

            $blockFive = $resultPage->getLayout()
                ->createBlock('Headboards\Catalog\Block\StepsWithOptions')
                ->setTemplate('Headboards_Catalog::product/view/step_five.phtml')
                ->setData('child_id', $headboardChildId)
                ->toHtml();

            $blockSix = $resultPage->getLayout()
                ->createBlock('Headboards\Catalog\Block\StepsWithOptions')
                ->setTemplate('Headboards_Catalog::product/view/step_six.phtml')
                ->setData('child_id', $headboardChildId)
                ->toHtml();

            $result->setData([
                'blockTwo' => $blockTwo,
                'blockThree' => $blockThree,
                'blockFour' => $blockFour,
                'blockFive' => $blockFive,
                'blockSix' => $blockSix,
                ]);

            return $result;
        }

        return null;

    }
}
