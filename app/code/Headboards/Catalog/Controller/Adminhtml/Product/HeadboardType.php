<?php
/**
 * Headboards Catalog Controller HeadboardType.
 *
 * @category  Headboards
 * @package   Headboards\Catalog
 * @author    Dmitry Mosaniuk
 * @copyright 2018 Dmitry_Mosaniuk
 */

namespace Headboards\Catalog\Controller\Adminhtml\Product;

/**
 * Class HeadboardType
 * @package Headboards\Catalog\Controller\Adminhtml\Product
 */
class HeadboardType extends \Magento\Catalog\Controller\Adminhtml\Product
{
    /**
     * Layout Factory
     *
     * @var \Magento\Framework\View\Result\LayoutFactory
     */
    protected $resultLayoutFactory;

    /**
     * Data constructor.
     *
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Catalog\Controller\Adminhtml\Product\Builder $productBuilder
     * @param \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Catalog\Controller\Adminhtml\Product\Builder $productBuilder,
        \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory
    ) {
        parent::__construct($context, $productBuilder);
        $this->resultLayoutFactory = $resultLayoutFactory;
    }

    /**
     * Get products grid and serializer block
     *
     * @return \Magento\Framework\View\Result\Layout
     */
    public function execute()
    {
        $this->productBuilder->build($this->getRequest());
        $resultLayout = $this->resultLayoutFactory->create();
        $resultLayout->getLayout()->getBlock('catalog.product.edit.tab.headboardtype')
            ->setProductsHeadboardType($this->getRequest()->getPost('products_headboardtype', null));

        return $resultLayout;
    }
}
