<?php
/**
 * Headboards Catalog block HeadboardTypeGrid.
 *
 * @category  Headboards
 * @package   Headboards\Catalog
 * @author    Dmitry Mosaniuk
 * @copyright 2018 Dmitry_Mosaniuk
 */

namespace Headboards\Catalog\Controller\Adminhtml\Product;

/**
 * Class HeadboardTypeGrid
 * @package Headboards\Catalog\Controller\Adminhtml\Product
 */
class HeadboardTypeGrid extends HeadboardType
{
    
}
