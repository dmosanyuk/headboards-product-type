<?php
/**
 * Headboards Catalog plugin RendererPlugin.
 *
 * @category  Headboards
 * @package   Headboards\Catalog
 * @author    Dmitry Mosaniuk
 * @copyright 2018 Dmitry_Mosaniuk
 */

namespace Headboards\Catalog\Plugin\Cart\Item;

/**
 * Class RendererPlugin
 * @package Headboards\Catalog\Plugin\Cart\Item
 */
class RendererPlugin
{
    /**
     * Product Repository Interface.
     *
     * @var $productRepository
     */
    protected $productRepository;

    /**
     * RendererPlugin constructor.
     *
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     */
    public function __construct(
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
    ) {
        $this->productRepository = $productRepository;
    }

    /**
     * Get checkout cart product url
     *
     * @param \Magento\Checkout\Block\Cart\Item\Renderer $subject
     * @param string $result
     *
     * @return string
     */
    public function afterGetProductUrl(\Magento\Checkout\Block\Cart\Item\Renderer $subject, $result)
    {
        $quoteItem = $subject->getItem();
        $parentId = $quoteItem->getData("parent_item_id");
        if ($parentId) {
            $parentProduct = $this->productRepository->getById($parentId);

            return $parentProduct->getProductUrl();
        }

        return $result;
    }
}
