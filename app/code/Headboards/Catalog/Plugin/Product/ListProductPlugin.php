<?php
/**
 * Headboards Catalog plugin ListProductPlugin.
 *
 * @category  Headboards
 * @package   Headboards\Catalog
 * @author    Dmitry Mosaniuk
 * @copyright 2018 Dmitry_Mosaniuk
 */

namespace Headboards\Catalog\Plugin\Product;

/**
 * Class ListProductPlugin
 * @package Headboards\Catalog\Plugin\Product
 */
class ListProductPlugin
{
    /**
     * Steps With Options.
     *
     * @var $stepsWithOptions
     */
    protected $stepsWithOptions;

    /**
     * Request Http.
     *
     * @var $stepsWithOptions
     */
    protected $request;

    /**
     * AmountPlugin constructor.
     *
     * @param \Magento\Framework\App\Request\Http
     * @param \Headboards\Catalog\Block\StepsWithOptions
     */
    public function __construct(
        \Magento\Framework\App\Request\Http $request,
        \Headboards\Catalog\Block\StepsWithOptions $stepsWithOptions
    ) {
        $this->request = $request;
        $this->stepsWithOptions = $stepsWithOptions;
    }

    /**
     * Set minimum price of headboard type child product for headboard type products
     *
     * @param \Magento\Catalog\Block\Product\ListProduct $subject
     * @param \Magento\Catalog\Model\Product $product
     */
    public function beforeGetProductPrice(\Magento\Catalog\Block\Product\ListProduct $subject, \Magento\Catalog\Model\Product $product)
    {

        if ($product->getTypeId() == \Headboards\Catalog\Model\Product\HeadboardType::TYPE_ID) {
            $headboardChildProductsMinPrice = $this->stepsWithOptions->getHeadboardChildProductsMinPrice($product);
            if ($headboardChildProductsMinPrice) {
                $product->setPrice($headboardChildProductsMinPrice);
            }
        }
    }
}
