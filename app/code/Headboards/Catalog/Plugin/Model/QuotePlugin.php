<?php
/**
 * Headboards Catalog plugin QuotePlugin.
 *
 * @category  Headboards
 * @package   Headboards\Catalog
 * @author    Dmitry Mosaniuk
 * @copyright 2018 Dmitry_Mosaniuk
 */

namespace Headboards\Catalog\Plugin\Model;

/**
 * Class QuotePlugin
 * @package Headboards\Catalog\Plugin\Model
 */
class QuotePlugin
{
    /**
     * Get all visible items with parent product id
     *
     * @param \Magento\Quote\Model\Quote $subject
     * @param array $result
     *
     * @return \Magento\Quote\Model\Quote\Item[]
     */
    public function afterGetAllVisibleItems(\Magento\Quote\Model\Quote $subject, $result)
    {
        $items = [];
        if ($subject->getItemsCollection()) {
            foreach ($subject->getItemsCollection() as $item) {
                if (!$item->isDeleted() && $item->getParentItemId()) {
                    $items[] = $item;
                }
            }

            return $items;
        }

        return $result;
    }
}
